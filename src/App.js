

import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import AppNavbar from './components/AppNavbar';
import {BrowserRouter as Router, Route, Routes, Switch, Redirect} from 'react-router-dom';
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Products from './pages/Products';
import Register from './pages/Register';
import './App.css';

function App() {

  return (


    <>
    <Router>
      <AppNavbar/>
      <Container>
        <Routes>
          <Route exact path="/" element={<Home/>}/>
          <Route exact path="/products" element={<Products/>}/>
          <Route exact path="/register" element={<Register/>}/>
          <Route exact path="/login" element={<Login/>}/>
          <Route exact path="*" element={<Error/>}/>
        </Routes>     
      </Container>
    </Router> 
    </>
  );
}

export default App;
