
import {Row, Col, Button} from 'react-bootstrap';
import '../App.css';

export default function Banner(){
	return (
		<Row>
			<Col className ="banner p-5">
					<h1>Treat Your Buddy</h1>
					<p>Treat your bestfriend by giving them their needs.</p>
					<Button variant="primary">Explore Products</Button>
			</Col>
		</Row>

	)
}