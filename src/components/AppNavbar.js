import {Navbar, Container, Nav} from 'react-bootstrap';
import '../App.css';

export default function AppNavBar(){
	return (
			<Navbar className="navbar" expand="lg">
		      <Container>
		        <Navbar.Brand href="#home">Pawssion</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="me-auto">
		            <Nav.Link href="#home">Home</Nav.Link>
		            <Nav.Link href="#link">Products</Nav.Link>
		            <Nav.Link href="#link">Register</Nav.Link>
		            <Nav.Link href="#link">Log In</Nav.Link>
		          </Nav>
		        </Navbar.Collapse>
		      </Container>
		    </Navbar>



		)

}