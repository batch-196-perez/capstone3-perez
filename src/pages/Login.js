import {Form, Button} from 'react-bootstrap';

export default function Login(){
	return(
		<>
		<h1>Log In</h1>
		<Form>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter your email here"
					required
					value = ""
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>	
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your password here"
					required
					value = ""
				/>
			</Form.Group>

				<Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">Log In
				</Button>

			}
		</Form>
		</>

	)
}