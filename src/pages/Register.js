import {Form, Button, Container} from 'react-bootstrap';

export default function Register(){
	return (
		<>
		<container className="regCon">
			<h1>Register Here: </h1>
			<Form className="form">
				<Form.Group className="form-input" controlId="firstName">
					<Form.Label>First Name</Form.Label>
					<Form.Control
						className = "forminput"
						type="text"
						placeholder="Enter your first name here"
						required
						value = ""
						
					/>
				</Form.Group>

				<Form.Group className="form-input" controlId="lastName">
					<Form.Label>Last Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter your last name here"
						required
						value = ""
						
					/>
				</Form.Group>

				<Form.Group className="form-input" controlId="mobileNo">
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter your 11-digit mobile number here"
						required
						value = ""
						
					/>
				</Form.Group>

				<Form.Group className="form-input" controlId="userEmail">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter your email here"
						required
						value = ""
						
					/>
					<Form.Text className="text-muted">
						We'll never share your email with anyone else.
					</Form.Text>	
				</Form.Group>

				<Form.Group className="form-input" controlId="password">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter your password here"
						required
						value = ""
						
					/>
				</Form.Group>
					<Button className="buttonko mt-3 mb-5" variant="success" type="submit" id="submitBtn">Register
					</Button>

			</Form>
		</container>
		</>


	)
}