

import productData from '../data/productData';
import ProductCard from '../components/ProductCard';

export default function Product(){

	const product = productData.map(product => {
		
		return(
			<ProductCard key={product.id} productProp = {product}/>
		)
	})

	return (
		//<ProductCard/>
		<>
		<h1> Available Courses: </h1>
		{product}
		</>
	)
}